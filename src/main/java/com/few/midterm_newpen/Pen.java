/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.few.midterm_newpen;

/**
 *
 * @author f
 */
public class Pen {

    protected char inkColor = 'B';
    protected double nibSize = 0.5;

    public Pen(char inkColor, double nibSize) {
        this.inkColor = inkColor;
        this.nibSize = nibSize;//nib = ขนาดของปลายปากกา
    }

    public boolean showPen(char penColor) {
        switch (penColor) {
            case 'B':
                Blue();
                break;
            case 'R':
                Red();
                break;
            case 'G':
                Green();
                break;
        }
        return true;
    }

    public boolean showPen() {
        return this.showPen(inkColor);
    }

    public void Blue() {
        System.out.println("Ink Color: Blue");
        System.out.println("Nib Size: " + nibSize);
        System.out.println("__________________");
    }

    public void Red() {
        System.out.println("Ink Color: Red");
        System.out.println("Nib Size: " + nibSize);
        System.out.println("__________________");
    }

    public void Green() {
        System.out.println("Ink Color: Green");
        System.out.println("Nib Size: " + nibSize);
        System.out.println("__________________");
    }
}
